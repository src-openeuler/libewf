Name:           libewf
Version:        20140608
Release:        16
Summary:        Library to access the Expert Witness Compression Format (EWF)
License:        LGPLv3+
URL:            http://sourceforge.net/projects/libewf/
Source0:        https://github.com/ArchAssault-Project/libewf/archive/v.20140608.tar.gz
Patch0:         libewf-ewfoutput-openssl3.patch
BuildRequires:  gcc-c++ fuse-devel libuuid-devel openssl-devel zlib-devel

%description
Libewf is a library for support of the Expert Witness Compression Format (EWF),
it support both the SMART format (EWF-S01) and the EnCase format (EWF-E01).
Libewf allows you to read and write media information within the EWF files.
libewf allows to read files created by EnCase 1 to 6, linen and FTK Imager.

%package -n     ewftools
Summary:        Tools for the Expert Witness Compression Format (EWF)
Requires:       libewf = %{version}-%{release}
Provides:       libewf-tools = %{version}-%{release}
Obsoletes:      libewf-tools <= %{version}-%{release}

%description -n ewftools
Several tools for reading and writing EWF files.It provides tools to acquire,
verify and export EWF files.

%package        devel
Summary:        Development files
Requires:       libewf = %{version}-%{release} zlib-devel pkgconfig

%description    devel
The libewf-devel package provides header files and libraries for developing
applications that use libewf.

%package        help
Summary:        Help documentation for libewf
BuildArch:      noarch

%description    help
This package contains man manual for help.

%prep
%autosetup -n libewf-v.20140608 -p1

%build
%configure --disable-static --enable-wide-character-type \
CFLAGS="${RPM_OPT_FLAGS} -std=gnu89"

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
sed -i -e 's! -shared ! -Wl,--as-needed\0!g' libtool

%make_build

%install
%make_install
%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc AUTHORS NEWS COPYING
%{_libdir}/*.so.*

%files -n ewftools
%{_bindir}/ewf*

%files devel
%{_includedir}/{libewf.h,libewf/}
%{_libdir}/*.so
%{_libdir}/pkgconfig/libewf.pc

%files help
%{_mandir}/man1/*.gz
%{_mandir}/man3/*.gz

%changelog
* Fri Feb 3 2023 caodongxia <caodongxia@h-partners.com> - 20140608-16
- Fix ewfoutput to compile with openssl-3.x

* Mon Oct 26 2020 leiju <leiju4@huawei.com> - 20140608-15
- Remove BuildRequires python2-devel and Requires python2-fuse

* Wed Sep 2 2020 zhangtao <zhangtao221@huawei.com> - 20140608-14
- Modify the Source0 Url

* Tue Feb 25 2020 Tianfei <tianfei16@huawei.com> - 20140608-13
- Package init
